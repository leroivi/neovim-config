-- PACKAGES

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  -- Language tools (LSP, Linter ...)
  {"nvim-treesitter/nvim-treesitter"},
  {"williamboman/mason.nvim"},
  {"williamboman/mason-lspconfig.nvim"},
  {"neovim/nvim-lspconfig"},
  -- Navigation
  {
    'nvim-telescope/telescope.nvim', branch = '0.1.x',
    dependencies = { 'nvim-lua/plenary.nvim', 'BurntSushi/ripgrep' }
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    branch = "v3.x",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
    }
  },
  -- Toolkit
  {
    'echasnovski/mini.nvim',
    version = '*'
  },
})

-- Configure 'mason'
require("mason").setup()
require("mason-lspconfig").setup()
require("mason-lspconfig").setup_handlers {
  -- The first entry (without a key) will be the default handler
  -- and will be called for each installed server that doesn't have
  -- a dedicated handler.
  function (server_name) -- default handler (optional)
    require("lspconfig")[server_name].setup {}
  end
}

-- Activate 'mini' modules
require("mini.comment").setup()
require("mini.completion").setup()
require("mini.indentscope").setup()
require("mini.map").setup()
require("mini.pairs").setup()
require("mini.surround").setup()
require("mini.trailspace").setup()

-- SETTINGS

-- Enable minimap
MiniMap.open()

-- Set highlight on search
vim.o.hlsearch = true

-- Make line numbers default
vim.wo.number = true

-- Enable mouse mode
vim.o.mouse = 'a'

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'

-- Save undo history
vim.o.undofile = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Indent
vim.o.tabstop = 2
vim.o.softtabstop = 2
vim.o.shiftwidth = 2
vim.o.shiftround = true
vim.o.expandtab = true
vim.o.autoindent = true
vim.o.smartindent = true
vim.o.smarttab = true

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- Enable spellchecking
vim.o.spelllang = 'en,fr'
vim.o.spell = false
vim.api.nvim_create_autocmd("FileType", {
  pattern = {"text","markdown","asciidoc","tex","latex"},
  callback = function(args)
    vim.opt_local.spell = true
  end
})

-- CUSTOM SHORTCUTS

local telescope = require('telescope.builtin')
vim.keymap.set('n', '<C-f>f', telescope.find_files, {})
vim.keymap.set('n', '<C-f>g', telescope.live_grep, {})
vim.keymap.set('n', '<C-f>b', telescope.buffers, {})
vim.keymap.set('n', '<C-f>h', telescope.help_tags, {})

-- CUSTOM COMMANDS

vim.api.nvim_create_user_command('EditNvimConfig', function(opts)
    vim.cmd.edit(vim.fn.stdpath('config') .. '/init.lua')
  end, {})
